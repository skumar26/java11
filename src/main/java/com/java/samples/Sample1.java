package com.java.samples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Sample1 {

	public static void main(String[] args) {

		var value = "text";
		Sample1 sample1 = new Sample1();
		sample1.handle();
	}

	private void handle() {
		var text = 32;
		System.err.println(text);
		var map = new HashMap<String, String>();
		map.put("A", "B");

		var myList = new ArrayList<Map<String, String>>();

		myList.add(map);

		for (var item : myList) {

			System.err.println(item);

		}

	}

}
